﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix_proyect_finall
{
    class Program
    {



        static void Main(string[] args)
        {
            //Cambia el color de la pantalla y el texto
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.ForegroundColor = ConsoleColor.Black;

            int salida = 0;
            int eleccion_menu = 0;
            //Menu del usuario
            do
            {
                // Mike_get_metodos sirve para traer todos los metodos 
                Program Mike_get_metodos = new Program();
                //Menu del programa 
                Console.WriteLine(" Hello. This is  Matrix Calculator  ");
                Console.WriteLine(" 1 Sum  ");
                Console.WriteLine(" 2 subtraction");
                Console.WriteLine(" 3 Multi ");
                Console.WriteLine(" 4 Gauss ");
                Console.WriteLine(" 5 Gauss Jordan  ");
                Console.WriteLine(" 6 Inverse matrix  ");
                Console.WriteLine(" 7 Cramer  ");
                Console.WriteLine(" 8 Sarrus  ");
                Console.WriteLine(" 9 Easy Cramer   ");
                Console.WriteLine(" 10 Cofactors matrix ");
                Console.WriteLine(" 11 Exit to *****   ");

                try
                {
                    // Aqui tomamos la eleccion del usuario 
                    Console.WriteLine(" Insert your choice ");
                    eleccion_menu = int.Parse(Console.ReadLine());

                    Console.Clear();
                    //Matriz de ejemplo
                    Console.WriteLine("La Matriz a resolver");
                    Console.WriteLine(" ---           ---");
                    Console.WriteLine("| X11 X12 X13 | T1 ");
                    Console.WriteLine("| X21 X22 X23 | T2 ");
                    Console.WriteLine("| X31 X32 X33 | T3 ");
                    Console.WriteLine(" ---           ---");

                    //Aqui se establecen los valores
                    Console.WriteLine("Insert element X11");
                    double M11 = double.Parse(Console.ReadLine());
                    Console.WriteLine("Insert element X12");
                    double M12 = double.Parse(Console.ReadLine());
                    Console.WriteLine("Insert element X13");
                    double M13 = double.Parse(Console.ReadLine());
                    Console.WriteLine("Insert element t1");
                    double R1 = double.Parse(Console.ReadLine());
                    Console.WriteLine("Insert element X21");
                    double M21 = double.Parse(Console.ReadLine());
                    Console.WriteLine("Insert element X22");
                    double M22 = double.Parse(Console.ReadLine());
                    Console.WriteLine("Insert element X23");
                    double M23 = double.Parse(Console.ReadLine());
                    Console.WriteLine("Insert element t2");
                    double R2 = double.Parse(Console.ReadLine());
                    Console.WriteLine("Insert element X31");
                    double M31 = double.Parse(Console.ReadLine());
                    Console.WriteLine("Insert element X32");
                    double M32 = double.Parse(Console.ReadLine());
                    Console.WriteLine("Insert element X33");
                    double M33 = double.Parse(Console.ReadLine());
                    Console.WriteLine("Insert element t3");
                    double R3 = double.Parse(Console.ReadLine());

                    // Con ayuda del switch le damos la opcion al usuario de escoger lo que quiera del menu
                    switch (eleccion_menu)
                    {

                        case 1:
                            Console.Clear();
                            Mike_get_metodos.Suma();

                            break;

                        case 2:
                            Console.Clear();
                            Mike_get_metodos.Resta();
                            break;
                        //MULTIPLICACION
                        case 3:

                            Mike_get_metodos.Multiplicacion(M11, M12, M13, M21, M22, M23, M31, M32, M33);

                            break;

                        //GAUSS
                        case 4:
                            Mike_get_metodos.Gauss(M11, M12, M13, R1, M21, M22, M23, R2, M31, M32, M33, R3);

                            break;

                        //GAUSS JORDAN
                        case 5:
                            Mike_get_metodos.Gauss_Joran(M11, M12, M13, R1, M21, M22, M23, R2, M31, M32, M33, R3);

                            break;

                        //INVERSAS
                        case 6:
                            Mike_get_metodos.Matriz_tipo_jordan(M11, M12, M13, R1, M21, M22, M23, R2, M31, M32, M33, R3);

                            break;
                        //CRAMER 
                        case 7:
                            Mike_get_metodos.Cramer(M11, M12, M13, R1, M21, M22, M23, R2, M31, M32, M33, R3);

                            break;

                        case 8:
                            Mike_get_metodos.Sarrus(M11, M12, M13, R1, M21, M22, M23, R2, M31, M32, M33, R3);

                            break;

                        case 9:
                            Mike_get_metodos.Easy_cramer(M11, M12, M13, R1, M21, M22, M23, R2, M31, M32, M33, R3);

                            break;

                        case 10:
                            Mike_get_metodos.Factors_matrix(M11, M12, M13, R1, M21, M22, M23, R2, M31, M32, M33, R3);
                            break;

                        default:

                            break;
                    }


                    Console.WriteLine(" ---           ---");
                    Console.WriteLine("1. Exit or anynumber to keeping  ");
                    salida = int.Parse(Console.ReadLine());
                    Console.Clear();

                }

                catch (Exception)
                {
                    Console.Clear();
                    Console.WriteLine("Please, Try it again, Do not be a BITCH ");
                }
            }
            while (salida != 1 || eleccion_menu != 10);
            Environment.Exit(1);

            Console.ReadKey();

        }

        void Suma()
        {   // Se puede expandir utilizando listas, no fue facil
            List<int> matrix_1 = new List<int>();
            List<int> matrix_2 = new List<int>();
            List<int> matrix_final = new List<int>();
            int row = 0;
            int column = 0;
            int row_2 = 0;
            int column_2 = 0;
            int guarda_valores = 0;

            Console.WriteLine(" Matrix 1 ");
            Console.WriteLine(" Insert number of Rows ");
            row = int.Parse(Console.ReadLine());

            Console.WriteLine(" Insert number of columns ");
            column = int.Parse(Console.ReadLine());

            int long_matrix_1 = row * column;

            for (int colector = 0; colector < long_matrix_1; colector++)
            {
                Console.WriteLine(" Insert element ");
                guarda_valores = int.Parse(Console.ReadLine());
                matrix_1.Add(guarda_valores);
            }

            Console.WriteLine(" Matrix 2 ");
            Console.WriteLine(" Insert number of Rows ");
            row_2 = int.Parse(Console.ReadLine());

            Console.WriteLine(" Insert number of columns ");
            column_2 = int.Parse(Console.ReadLine());

            int long_matrix_2 = row_2 * column_2;

            for (int colector = 0; colector < long_matrix_2; colector++)
            {
                Console.WriteLine(" Insert element ");
                guarda_valores = int.Parse(Console.ReadLine());
                matrix_2.Add(guarda_valores);
            }


            for (int i = 0; i < matrix_1.Count; i++)
            {

                matrix_final.Add(matrix_1[i] + matrix_2[i]);

            }

            Console.WriteLine(" ---           ---");
            foreach (int item in matrix_final)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine(" ---           ---");


        }

        void Resta()
        {
            // Funciona de la misma manera que el metodo de Suma pero esto es una Resta
            List<int> matrix_1 = new List<int>();
            List<int> matrix_2 = new List<int>();
            List<int> matrix_final = new List<int>();
            int row = 0;
            int column = 0;
            int row_2 = 0;
            int column_2 = 0;
            int guarda_valores = 0;

            Console.WriteLine(" Matrix 1 ");
            Console.WriteLine(" Insert number of Rows ");
            row = int.Parse(Console.ReadLine());

            Console.WriteLine(" Insert number of columns ");
            column = int.Parse(Console.ReadLine());

            //Establece el tamano de la matriz
            int long_matrix_1 = row * column;

            for (int colector = 0; colector < long_matrix_1; colector++)
            {
                Console.WriteLine(" Insert element ");
                guarda_valores = int.Parse(Console.ReadLine());
                matrix_1.Add(guarda_valores);
            }

            Console.WriteLine(" Matrix 2 ");
            Console.WriteLine(" Insert number of Rows ");
            row_2 = int.Parse(Console.ReadLine());

            Console.WriteLine(" Insert number of columns ");
            column_2 = int.Parse(Console.ReadLine());

            int long_matrix_2 = row_2 * column_2;

            for (int colector = 0; colector < long_matrix_2; colector++)
            {
                Console.WriteLine(" Insert element ");
                guarda_valores = int.Parse(Console.ReadLine());
                matrix_2.Add(guarda_valores);
            }
            for (int i = 0; i < matrix_1.Count; i++)
            {

                matrix_final.Add(matrix_1[i] - matrix_2[i]);

            }

            Console.WriteLine(" ---           ---");
            foreach (int item in matrix_final)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine(" ---           ---");
        }

        void Multiplicacion(double x11, double x12, double x13, double x21,
            double x22, double x23, double x31, double x32, double x33)
        {
            Console.Clear();
            Console.WriteLine(" ---           ---");
            Console.WriteLine("|  x11   x12  x13  | x14   x15  x16");
            Console.WriteLine("|  x21   x22  x23  | x24   x25  x26");
            Console.WriteLine("|   x31  x32  x33  | x34   x35  x36");
            Console.WriteLine(" ---           ---");

            double z1, z2, z3, z4, z5, z6, z7, z8, z9;

            Console.WriteLine("Insert element X14");
            double x14 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element X15");
            double x15 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element X16");
            double x16 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element X24");
            double x24 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element x25");
            double x25 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element X26");
            double x26 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element  x34");
            double x34 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element x35");
            double x35 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element x36");
            double x36 = double.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine(" ---           ---");
            Console.WriteLine("|  " + x11 + "  " + x12 + "  " + x13 + "|" + x14 + "  " + x15 + "  " + x16);
            Console.WriteLine("|  " + x21 + "  " + x22 + "  " + x23 + "|" + x24 + "  " + x25 + "  " + x26);
            Console.WriteLine("|  " + x31 + "  " + x32 + "  " + x33 + "|" + x34 + "  " + x35 + "  " + x36);
            Console.WriteLine(" ---           ---");

            z1 = (x11 * x14) + (x12 * x24) + (x13 * x34);
            z2 = (x11 * x15) + (x12 * x25) + (x13 * x35);
            z3 = (x11 * x16) + (x12 * x26) + (x13 * x36);

            z4 = (x21 * x14) + (x22 * x24) + (x23 * x34);
            z5 = (x21 * x15) + (x22 * x25) + (x23 * x35);
            z6 = (x21 * x16) + (x22 * x26) + (x23 * x36);

            z7 = (x31 * x14) + (x32 * x24) + (x33 * x34);
            z8 = (x31 * x15) + (x32 * x25) + (x33 * x35);
            z9 = (x31 * x16) + (x32 * x26) + (x33 * x36);

            Console.WriteLine(" ---           ---");
            Console.WriteLine("|  " + z1 + "  " + z2 + "  " + z3);
            Console.WriteLine("|  " + z4 + "  " + z5 + "  " + z6);
            Console.WriteLine("|  " + z7 + "  " + z8 + "  " + z9);
            Console.WriteLine(" ---           ---");

        }

        void Gauss(double x11, double x12, double x13, double t1, double x21,
            double x22, double x23, double t2, double x31, double x32, double x33, double t3)
        {
            Console.Clear();
            double divi, multi, divis;
            double x, y, z;

            divi = x11;
            //aqui transformamos el valor de x11 en 1
            if (x11 > 0)
            {
                x11 = (x11 / divi);
                x12 = (x12 / divi);
                x13 = (x13 / divi);
                t1 = (t1 / divi);
            }
            if (x11 < 0)
            {
                x11 = (x11 / divi);
                x12 = (x12 / divi);
                x13 = (x13 / divi);
                t1 = (t1 / divi);
            }

            multi = x21;
            //aqui transformacion el valor x21 en 0
            if (x21 != 0)
            {
                if (x21 < 0)
                {
                    x21 = (multi * x11) - x21;
                    x22 = (multi * x12) - x22;
                    x23 = (multi * x13) - x23;
                    t2 = (multi * t1) - t2;
                }
                if (x21 > 0)
                {
                    x21 = (-multi * x11) + x21;
                    x22 = (-multi * x12) + x22;
                    x23 = (-multi * x13) + x23;
                    t2 = (-multi * t1) + t2;
                }
            }

            multi = x31;
            //aqui transformamos x31 en 0
            if (x31 != 0)
            {
                if (x31 < 0)
                {
                    x31 = (multi * x11) - x31;
                    x32 = (multi * x12) - x32;
                    x33 = (multi * x13) - x33;
                    t3 = (multi * t1) - t3;
                }
                if (x31 > 0)
                {
                    x31 = (-multi * x11) + x31;
                    x32 = (-multi * x12) + x32;
                    x33 = (-multi * x13) + x33;
                    t3 = (-multi * t1) + t3;
                }
            }

            divis = x22;
            //aqui transformamos x22 en 1
            if (x22 > 1)
            {
                x21 = (x21 / divis);
                x22 = (x22 / divis);
                x23 = (x23 / divis);
                t2 = (t2 / divis);
            }
            if (x22 < 1)
            {
                x21 = (x21 / divis);
                x22 = (x22 / divis);
                x23 = (x23 / divis);
                t2 = (t2 / divis);
            }

            multi = x32;
            //aqui trasnformamos x32 en 0
            if (x32 != 0)
            {
                if (x32 < 0)
                {
                    x31 = (multi * x21) - x31;
                    x32 = (multi * x22) - x32;
                    x33 = (multi * x23) - x33;
                    t3 = (multi * t2) - t3;
                }
                if (x32 > 0)
                {
                    x31 = (-multi * x21) + x31;
                    x32 = (-multi * x22) + x32;
                    x33 = (-multi * x23) + x33;
                    t3 = (-multi * t2) + t3;
                }
            }

            divis = x33;
            //aqui dice que si los valores x33 y el t3 son 0 son homogenea
            if (t3 == 0 && x33 == 0)
            {

            }
            else
            {
                //aqui transformamos el valor x33 en 1
                if (x33 > 1)
                {
                    x31 = (x31 / divis);
                    x32 = (x32 / divis);
                    x33 = (x33 / divis);
                    t3 = (t3 / divis);
                }
                if (x33 < 1)
                {
                    x31 = (x31 / divis);
                    x32 = (x32 / divis);
                    x33 = (x33 / divis);
                    t3 = (t3 / divis);
                }
            }

            Console.WriteLine(" ---           ---");
            Console.WriteLine("|  " + x11 + "  " + x12 + "  " + x13 + " | " + t1 + " ");
            Console.WriteLine("|  " + x21 + "  " + x22 + "  " + x23 + " | " + t2 + " ");
            Console.WriteLine("|  " + x31 + "  " + x32 + "  " + x33 + " | " + t3 + " ");
            Console.WriteLine(" ---           ---");

            Console.WriteLine(" ---           ---");
            z = t3;
            Console.WriteLine("Z = " + z);
            y = t2 - (x23 * z);
            Console.WriteLine("Y = " + y);
            x = t1 - ((x12 * y) + (x13 * z));
            Console.WriteLine("X = " + x);
            Console.WriteLine(" ---           ---");


        }

        void Gauss_Joran(double x11, double x12, double x13, double t1, double x21,
            double x22, double x23, double t2, double x31, double x32, double x33, double t3)
        {
            Console.Clear();
            double divi, multi, divis;

            divi = x11;

            //aqui transformamos el valor de x11 en 1
            if (x11 > 0)
            {
                x11 = (x11 / divi);
                x12 = (x12 / divi);
                x13 = (x13 / divi);
                t1 = (t1 / divi);
            }
            if (x11 < 0)
            {
                x11 = (x11 / divi);
                x12 = (x12 / divi);
                x13 = (x13 / divi);
                t1 = (t1 / divi);
            }

            multi = x21;
            //aqui transformacion el valor x21 en 0
            if (x21 != 0)
            {
                if (x21 < 0)
                {
                    x21 = (multi * x11) - x21;
                    x22 = (multi * x12) - x22;
                    x23 = (multi * x13) - x23;
                    t2 = (multi * t1) - t2;
                }
                if (x21 > 0)
                {
                    x21 = (-multi * x11) + x21;
                    x22 = (-multi * x12) + x22;
                    x23 = (-multi * x13) + x23;
                    t2 = (-multi * t1) + t2;
                }
            }

            multi = x31;
            //aqui transformamos x31 en 0
            if (x31 != 0)
            {
                if (x31 < 0)
                {
                    x31 = (multi * x11) - x31;
                    x32 = (multi * x12) - x32;
                    x33 = (multi * x13) - x33;
                    t3 = (multi * t1) - t3;
                }
                if (x31 > 0)
                {
                    x31 = (-multi * x11) + x31;
                    x32 = (-multi * x12) + x32;
                    x33 = (-multi * x13) + x33;
                    t3 = (-multi * t1) + t3;
                }
            }

            divis = x22;
            //aqui transformamos x22 en 1
            if (x22 > 1)
            {
                x21 = (x21 / divis);
                x22 = (x22 / divis);
                x23 = (x23 / divis);
                t2 = (t2 / divis);
            }
            if (x22 < 1)
            {
                x21 = (x21 / divis);
                x22 = (x22 / divis);
                x23 = (x23 / divis);
                t2 = (t2 / divis);
            }

            multi = x32;
            //aqui trasnformamos x32 en 0
            if (x32 != 0)
            {
                if (x32 < 0)
                {
                    x31 = (multi * x21) - x31;
                    x32 = (multi * x22) - x32;
                    x33 = (multi * x23) - x33;
                    t3 = (multi * t2) - t3;
                }
                if (x32 > 0)
                {
                    x31 = (-multi * x21) + x31;
                    x32 = (-multi * x22) + x32;
                    x33 = (-multi * x23) + x33;
                    t3 = (-multi * t2) + t3;
                }
            }

            multi = x12;
            //aqui trasnformamos x12 en 0
            if (x12 != 0)
            {
                if (x12 < 0)
                {

                    x12 = (multi * x22) - x12;
                    x13 = (multi * x23) - x13;
                    t1 = (multi * t2) - t1;

                }

                if (x12 > 0)
                {
                    x12 = (-multi * x22) + x12;
                    x13 = (-multi * x23) + x13;
                    t1 = (-multi * t2) + t1;
                }

            }

            divis = x33;
            //aqui dice que si los valores x33 y el t3 son 0 son homogenea
            if (t3 == 0 && x33 == 0)
            {

            }
            else
            {
                //aqui transformamos el valor x33 en 1
                if (x33 > 1)
                {
                    x31 = (x31 / divis);
                    x32 = (x32 / divis);
                    x33 = (x33 / divis);
                    t3 = (t3 / divis);
                }
                if (x33 < 1)
                {
                    x31 = (x31 / divis);
                    x32 = (x32 / divis);
                    x33 = (x33 / divis);
                    t3 = (t3 / divis);
                }
            }

            multi = x13;
            //aqui trasnformamos x12 en 0
            if (x13 != 0)
            {
                if (x13 < 0)
                {

                    x13 = (multi * x33) - x13;
                    t1 = (multi * t3) - t1;

                }

                if (x13 > 0)
                {

                    x13 = (-multi * x33) + x13;
                    t1 = (-multi * t3) + t1;
                }

            }


            multi = x23;
            //aqui trasnformamos x12 en 0
            if (x23 != 0)
            {
                if (x23 < 0)
                {

                    x23 = (multi * x33) - x23;
                    t2 = (multi * t3) - t2;

                }

                if (x23 > 0)
                {

                    x23 = (-multi * x33) + x23;
                    t2 = (-multi * t3) + t2;
                }

            }
            Console.Clear();
            Console.WriteLine(" ---           ---");
            Console.WriteLine("|  " + x11 + "  " + x12 + "  " + x13 + " | " + t1 + " ");
            Console.WriteLine("|  " + x21 + "  " + x22 + "  " + x23 + " | " + t2 + " ");
            Console.WriteLine("|  " + x31 + "  " + x32 + "  " + x33 + " | " + t3 + " ");
            Console.WriteLine(" ---           ---");

        }

        void Matriz_tipo_jordan(double x11, double x12, double x13, double t1, double x21,
            double x22, double x23, double t2, double x31, double x32, double x33, double t3)
        {
            double divi, multi, divis;
            double x, y, z;
            double x0 = 1, x1 = 0, x2 = 0, x3 = 0, x4 = 1, x5 = 0, x6 = 0, x7 = 0, x8 = 1;
            Console.WriteLine(" ---           ---");
            Console.WriteLine("|  " + x11 + "  " + x12 + "  " + x13 + "|" + x0 + "  " + x1 + "  " + x2);
            Console.WriteLine("|  " + x21 + "  " + x22 + "  " + x23 + "|" + x3 + "  " + x4 + "  " + x5);
            Console.WriteLine("|  " + x31 + "  " + x32 + "  " + x33 + "|" + x6 + "  " + x7 + "  " + x8);
            Console.WriteLine(" ---           ---");

            divi = x11;

            //aqui transformamos el valor de x11 en 1
            if (x11 > 0)
            {
                x11 = (x11 / divi);
                x12 = (x12 / divi);
                x13 = (x13 / divi);
                x0 = (x0 / divi);
                x1 = (x1 / divi);
                x2 = (x2 / divi);
            }
            if (x11 < 0)
            {
                x11 = (x11 / divi);
                x12 = (x12 / divi);
                x13 = (x13 / divi);
                x0 = (x0 / divi);
                x1 = (x1 / divi);
                x2 = (x2 / divi);

            }

            multi = x21;
            //aqui transformacion el valor x21 en 0
            if (x21 != 0)
            {
                if (x21 < 0)
                {
                    x21 = (multi * x11) - x21;
                    x22 = (multi * x12) - x22;
                    x23 = (multi * x13) - x23;
                    x3 = (multi * x0) - x3;
                    x4 = (multi * x1) - x4;
                    x5 = (multi * x2) - x5;

                }
                if (x21 > 0)
                {
                    x21 = (-multi * x11) + x21;
                    x22 = (-multi * x12) + x22;
                    x23 = (-multi * x13) + x23;
                    x3 = (-multi * x0) + x3;
                    x4 = (-multi * x1) + x4;
                    x5 = (-multi * x2) + x5;

                }
            }

            multi = x31;
            //aqui transformamos x31 en 0
            if (x31 != 0)
            {
                if (x31 < 0)
                {
                    x31 = (multi * x11) - x31;
                    x32 = (multi * x12) - x32;
                    x33 = (multi * x13) - x33;
                    x6 = (multi * x0) - x6;
                    x7 = (multi * x1) - x7;
                    x8 = (multi * x2) - x8;


                }
                if (x31 > 0)
                {
                    x31 = (-multi * x11) + x31;
                    x32 = (-multi * x12) + x32;
                    x33 = (-multi * x13) + x33;
                    x6 = (-multi * x0) + x6;
                    x7 = (-multi * x1) + x7;
                    x8 = (-multi * x2) + x8;

                }
            }

            divis = x22;
            //aqui transformamos x22 en 1
            if (x22 > 1)
            {
                x21 = (x21 / divis);
                x22 = (x22 / divis);
                x23 = (x23 / divis);
                x3 = (x3 / divis);
                x4 = (x4 / divis);
                x5 = (x5 / divis);

            }

            if (x22 < 1)
            {
                x21 = (x21 / divis);
                x22 = (x22 / divis);
                x23 = (x23 / divis);
                x3 = (x3 / divis);
                x4 = (x4 / divis);
                x5 = (x5 / divis);

            }

            multi = x32;
            //aqui trasnformamos x32 en 0
            if (x32 != 0)
            {
                if (x32 < 0)
                {
                    x31 = (multi * x21) - x31;
                    x32 = (multi * x22) - x32;
                    x33 = (multi * x23) - x33;
                    x6 = (multi * x3) - x6;
                    x7 = (multi * x4) - x7;
                    x8 = (multi * x5) - x8;



                }
                if (x32 > 0)
                {
                    x31 = (-multi * x21) + x31;
                    x32 = (-multi * x22) + x32;
                    x33 = (-multi * x23) + x33;
                    x6 = (-multi * x3) + x6;
                    x7 = (-multi * x4) + x7;
                    x8 = (-multi * x5) + x8;

                }
            }

            multi = x12;
            //aqui trasnformamos x12 en 0
            if (x12 != 0)
            {
                if (x12 < 0)
                {

                    x12 = (multi * x22) - x12;
                    x13 = (multi * x23) - x13;
                    x0 = (multi * x3) - x0;
                    x1 = (multi * x4) - x1;
                    x2 = (multi * x5) - x2;

                }

                if (x12 > 0)
                {
                    x12 = (-multi * x22) + x12;
                    x13 = (-multi * x23) + x13;
                    x0 = (-multi * x3) + x0;
                    x1 = (-multi * x4) + x1;
                    x2 = (-multi * x5) + x2;

                }

            }

            divis = x33;

            if (x33 > 1)
            {
                x31 = (x31 / divis);
                x32 = (x32 / divis);
                x33 = (x33 / divis);
                x6 = (x6 / divis);
                x7 = (x7 / divis);
                x8 = (x8 / divis);

            }


            if (x33 < 1)
            {
                x31 = (x31 / divis);
                x32 = (x32 / divis);
                x33 = (x33 / divis);
                x6 = (x6 / divis);
                x7 = (x7 / divis);
                x8 = (x8 / divis);

            }


            multi = x13;
            //aqui trasnformamos x12 en 0
            if (x13 != 0)
            {
                if (x13 < 0)
                {

                    x13 = (multi * x33) - x13;
                    x0 = (multi * x6) - x0;
                    x1 = (multi * x7) - x1;
                    x2 = (multi * x8) - x2;



                }

                if (x13 > 0)
                {

                    x13 = (-multi * x33) + x13;
                    x0 = (-multi * x6) + x0;
                    x1 = (-multi * x7) + x1;
                    x2 = (-multi * x8) + x2;

                }

            }


            multi = x23;
            //aqui trasnformamos x12 en 0
            if (x23 != 0)
            {
                if (x23 < 0)
                {

                    x23 = (multi * x33) - x23;
                    x3 = (multi * x6) - x3;
                    x4 = (multi * x7) - x4;
                    x5 = (multi * x8) - x5;


                }

                if (x23 > 0)
                {

                    x23 = (-multi * x33) + x23;
                    x3 = (-multi * x6) + x3;
                    x4 = (-multi * x7) + x4;
                    x5 = (-multi * x8) + x5;

                }

            }

            Console.WriteLine(" ---           ---");
            Console.WriteLine("|  " + x11 + "  " + x12 + "  " + x13 + "|" + x0 + "  " + x1 + "  " + x2);
            Console.WriteLine("|  " + x21 + "  " + x22 + "  " + x23 + "|" + x3 + "  " + x4 + "  " + x5);
            Console.WriteLine("|  " + x31 + "  " + x32 + "  " + x33 + "|" + x6 + "  " + x7 + "  " + x8);
            Console.WriteLine(" ---           ---");

            x = (x0 * t1) + (x1 * t2) + (x2 * t3);
            y = (x3 * t1) + (x4 * t2) + (x5 * t3);
            z = (x6 * t1) + (x7 * t2) + (x8 * t3);

            Console.WriteLine("x = " + x);
            Console.WriteLine("y = " + y);
            Console.WriteLine("z = " + z);

        }

        void Cramer(double x11, double x12, double x13, double t1, double x21,
            double x22, double x23, double t2, double x31, double x32, double x33, double t3)
        {
            double Ds, Dx, Dy, Dz, D1, D2, D3;

            Ds = (x11 * x22 * x33 + x12 * x23 * x31 + x13 * x21 * x32) - (x12 * x21 * x33 + x11 * x23 * x32 + x13 * x22 * x31);

            D1 = (t1 * x22 * x33 + x12 * x23 * t3 + x13 * t2 * x32) - (x12 * t2 * x33 + t1 * x23 * x32 + x13 * x22 * t3);
            Dx = (D1 / Ds);

            D2 = (x11 * t2 * x33 + t1 * x23 * x31 + x13 * x21 * t3) - (t1 * x21 * x33 + x11 * x23 * t3 + x13 * t2 * x31);
            Dy = (D2 / Ds);

            D3 = (x11 * x22 * t3 + x12 * t2 * x31 + t1 * x21 * x32) - (x12 * x21 * t3 + x11 * t2 * x32 + t1 * x22 * x31);
            Dz = (D3 / Ds);
            Console.Clear();
            Console.WriteLine("DS = " + Ds);
            Console.WriteLine("x = " + Dx);
            Console.WriteLine("y = " + Dy);
            Console.WriteLine("z = " + Dz);
        }

        void Sarrus(double x11, double x12, double x13, double t1, double x21,
            double x22, double x23, double t2, double x31, double x32, double x33, double t3)
        {
            double Ds, Dx, Dy, Dz, D1, D2, D3;

            Ds = (x11 * x22 * x33 + x21 * x32 * x13 + x31 * x12 * x23) - (x13 * x22 * x31 + x23 * x32 * x11 + x33 * x12 * x21);

            D1 = (t1 * x22 * x33 + t2 * x32 * x13 + t3 * x12 * x23) - (x13 * x22 * t3 + x23 * x32 * t1 + x33 * x12 * t2);
            Dx = (D1 / Ds);

            D2 = (x11 * t2 * x33 + x21 * t3 * x13 + x31 * t1 * x23) - (x13 * t2 * x31 + x23 * t3 * x11 + x33 * t1 * x21);
            Dy = (D2 / Ds);

            D3 = (x11 * x22 * t3 + x21 * x32 * t1 + x31 * x12 * t2) - (t1 * x22 * x31 + t2 * x32 * x11 + t3 * x12 * x21);
            Dz = (D3 / Ds);

            Console.Clear();
            Console.WriteLine("DS = " + Ds);
            Console.WriteLine("x = " + Dx);
            Console.WriteLine("y = " + Dy);
            Console.WriteLine("z = " + Dz);
        }

        void Easy_cramer(double x11, double x12, double x13, double t1, double x21,
            double x22, double x23, double t2, double x31, double x32, double x33, double t3)
        {
            double z1, z2, z3, z4, z5, z6, z7, z8, z9, z10, z11, z12, z13, z14, z15, z16;
            double Ds, Dx, Dy, Dz, Dt;
            double m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15, m16;
            double x14, x34, x24, x41, x42, x43, x44, t4;
            Console.Clear();
            Console.WriteLine(" ---               ---");
            Console.WriteLine("|  x11  x12  x13 x14 ");
            Console.WriteLine("|  x21  x22  x23  x24 ");
            Console.WriteLine("|  x31  x32  x33  x34 ");
            Console.WriteLine("|  x41  +x42 x43  x44 ");
            Console.WriteLine(" ---                ---");

            Console.WriteLine("Insert element X14");
            x14 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element X24");
            x24 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element X34");
            x34 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element X41");
            x41 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element x42");
            x42 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element X43");
            x43 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element x44");
            x44 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element t4");
            t4 = double.Parse(Console.ReadLine());

            Ds = (x11 * x22 * x33 * x44 + x21 * x32 * x43 * x14 + x31 * x42 * x13 * x24 + x41 * x12 * x23 * x34) - (x14 * x23 * x32 * x41 + x24 * x33 * x42 * x11 + x34 * x43 * x12 * x21 + x44 * x13 * x22 * x31);

            if (Ds == 0)
            {
                Console.WriteLine("Bye");
            }
            else
            {
                m1 = (x22 * x33 * x44 + x32 * x43 * x24 + x42 * x23 * x34) - (x24 * x33 * x42 + x34 * x43 * x22 + x44 * x23 * x32);
                z1 = (m1 * 1) / Ds;
                m2 = (x21 * x33 * x44 + x31 * x43 * x24 + x41 * x23 * x34) - (x24 * x33 * x41 + x34 * x43 * x21 + x44 * x23 * x31);
                z2 = (m2 * -1) / Ds;
                m3 = (x21 * x32 * x44 + x31 * x42 * x24 + x41 * x22 * x34) - (x24 * x32 * x41 + x34 * x42 * x21 + x44 * x22 * x31);
                z3 = (m3 * 1) / Ds;
                m4 = (x21 * x32 * x43 + x31 * x42 * x23 + x41 * x22 * x33) - (x23 * x32 * x41 + x33 * x42 * x21 + x43 * x22 * x31);
                z4 = (m4 * -1) / Ds;
                m5 = (x12 * x33 * x44 + x32 * x43 * x14 + x42 * x13 * x34) - (x14 * x33 * x42 + x34 * x43 * x12 + x44 * x13 * x32);
                z5 = (m5 * -1) / Ds;
                m6 = (x11 * x33 * x44 + x31 * x43 * x14 + x41 * x13 * x34) - (x14 * x33 * x41 + x34 * x43 * x11 + x44 * x13 * x31);
                z6 = (m6 * 1) / Ds;
                m7 = (x11 * x32 * x44 + x31 * x42 * x14 + x41 * x12 * x34) - (x14 * x32 * x41 + x34 * x42 * x11 + x44 * x12 * x31);
                z7 = (m7 * -1) / Ds;
                m8 = (x11 * x32 * x43 + x31 * x42 * x13 + x41 * x12 * x33) - (x13 * x32 * x41 + x33 * x42 * x11 + x43 * x12 * x31);
                z8 = (m8 * 1) / Ds;
                m9 = (x12 * x23 * x44 + x22 * x43 * x14 + x42 * x13 * x24) - (x14 * x23 * x42 + x24 * x43 * x12 + x44 * x13 * x22);
                z9 = (m9 * 1) / Ds;
                m10 = (x11 * x23 * x44 + x21 * x43 * x14 + x41 * x13 * x24) - (x14 * x23 * x41 + x24 * x43 * x11 + x44 * x13 * x21);
                z10 = (m10 * -1) / Ds;
                m11 = (x11 * x22 * x44 + x21 * x42 * x14 + x41 * x12 * x24) - (x14 * x22 * x41 + x24 * x42 * x11 + x44 * x12 * x21);
                z11 = (m11 * 1) / Ds;
                m12 = (x11 * x22 * x43 + x21 * x42 * x13 + x41 * x12 * x23) - (x13 * x22 * x41 + x23 * x42 * x11 + x43 * x12 * x21);
                z12 = (m12 * -1) / Ds;
                m13 = (x12 * x23 * x34 + x22 * x33 * x14 + x32 * x13 * x24) - (x14 * x23 * x32 + x24 * x33 * x12 + x34 * x13 * x22);
                z13 = (m13 * -1) / Ds;
                m14 = (x11 * x23 * x34 + x21 * x33 * x14 + x31 * x13 * x24) - (x14 * x23 * x31 + x24 * x33 * x11 + x34 * x13 * x21);
                z14 = (m14 * 1) / Ds;
                m15 = (x11 * x22 * x34 + x21 * x32 * x14 + x31 * x12 * x24) - (x14 * x22 * x31 + x24 * x32 * x11 + x34 * x12 * x21);
                z15 = (m15 * -1) / Ds;
                m16 = (x11 * x22 * x33 + x21 * x32 * x13 + x31 * x12 * x23) - (x13 * x22 * x31 + x23 * x32 * x11 + x33 * x12 * x21);
                z16 = (m16 * 1) / Ds;

                Dx = z1 * t1 + z5 * t2 + z9 * t3 + z13 * t4;
                Dy = z2 * t1 + z6 * t2 + z10 * t3 + z14 * t4;
                Dz = z3 * t1 + z7 * t2 + z11 * t3 + z15 * t4;
                Dt = z4 * t1 + z8 * t2 + z12 * t3 + z16 * t4;

                Console.WriteLine(" ---           ---");
                Console.WriteLine("| " + z1 + " " + z5 + " " + z9 + " " + z13 + " ");
                Console.WriteLine("| " + z2 + " " + z6 + " " + z10 + " " + z14 + " ");
                Console.WriteLine("| " + z3 + " " + z7 + " " + z11 + " " + z15 + " ");
                Console.WriteLine("| " + z4 + " " + z8 + " " + z12 + " " + z16 + " ");
                Console.WriteLine(" ---           ---");

                Console.WriteLine("Ds = " + Ds);
                Console.WriteLine("x = " + Dx);
                Console.WriteLine("y = " + Dy);
                Console.WriteLine("z = " + Dz);
                Console.WriteLine("t = " + Dt);

            }

        }

        void Factors_matrix(double x11, double x12, double x13, double t1, double x21,
            double x22, double x23, double t2, double x31, double x32, double x33, double t3)
        {
            double x14, x34, x24, x41, x42, x43, x44, t4;
            double Ds, Dx, Dy, Dz, Dt;
            double m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15, m16;
            Console.Clear();
            Console.WriteLine(" ---               ---");
            Console.WriteLine("|  x11  x12  x13 x14 ");
            Console.WriteLine("|  x21  x22  x23  x24 ");
            Console.WriteLine("|  x31  x32  x33  x34 ");
            Console.WriteLine("|  x41  +x42 x43  x44 ");
            Console.WriteLine(" ---                ---");

            Console.WriteLine("Insert element X14");
            x14 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element X24");
            x24 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element X34");
            x34 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element X41");
            x41 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element x42");
            x42 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element X43");
            x43 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element x44");
            x44 = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert element t4");
            t4 = double.Parse(Console.ReadLine());

            Ds = ( (1 * x11)   * ( (x22 * x33 * x44 + x32 * x43 * x24 + x42 * x23 * x34) - (x24 * x33 * x42 + x34 * x43 * x22 + x44 * x23 * x32) ) ) +
                 ( (-1 * x12)  * ( (x21 * x33 * x44 + x31 * x43 * x24 + x41 * x23 * x34) - (x24 * x33 * x41 + x34 * x43 * x21 + x44 * x23 * x31) ) ) +
                 ( (1  *  x13) * ( (x21 * x32 * x44 + x31 * x42 * x24 + x41 * x22 * x34) - (x24 * x32 * x41 + x34 * x42 * x21 + x44 * x22 * x31) ) ) +
                 ( (-1 * x14 ) * ( (x21 * x32 * x43 + x31 * x42 * x23 + x41 * x22 * x33) - (x23 * x32 * x41 + x33 * x42 * x21 + x43 * x22 * x31) ) ) ;

            Dx = ( (1 * t1)   * ( (x22 * x33 * x44 + x32 * x43 * x24 + x42 * x23 * x34) - (x24 * x33 * x42 + x34 * x43 * x22 + x44 * x23 * x32) ) ) +
                 ( (-1 * x12) * ( (t2 * x33 * x44 + t3 * x43 * x24 + t4 * x23 * x34) - (x24 * x33 * t4 + x34 * x43 * t2 + x44 * x23 * t3) ) ) +
                 ( ( 1 * x13) * ( (t2 * x32 * x44 + t3 * x42 * x24 + t4 * x22 * x34) - (x24 * x32 * t4 + x34 * x42 * t2 + x44 * x22 * t3) ) ) +
                 ( (-1 * x14) * ( (t2 * x32 * x43 + t3 * x42 * x23 + t4 * x22 * x33) - (x23 * x32 * t4 + x33 * x42 * t2 + x43 * x22 * t3) ) ) ;
            Dx = Dx / Ds;

            Dy = ( (1 * x11)  * ( (t2 * x33 * x44 + t3 * x43 * x24 + t4 * x23 * x34) - (x24 * x33 * t4  +  x34 * x43 * t2 +  x44 * x23 * t3) ) ) +
                 ( (-1 * t1 ) * ( (x21 * x33 * x44 + x31 * x43 * x24 + x41 * x23 * x34) - (x24 * x33 * x41 +  x34 * x43 * x21 +  x44 * x23 * x31) ) ) +
                 ( ( 1 * x13) * ( (x21 * t3 * x44 + x31 * t4 * x24 + x41 * t2 * x34) - (x24 * t3 * x41 +  x34 * t4 * x21 +  x44 * t2 * x31) ) ) +
                 ( (-1 * x14) * ( (x21 * t3 * x43 + x31 * t4 * x23 + x41 * t2 * x33) - (x23 * t3 * x41 +  x33 * t4 * x21 +  x43 * t2 * x31) ) ) ;
            Dy = Dy / Ds;

            Dz = ( (1 * x11)  * ( (x22 * t3 * x44 + x32 * t4 * x24 + x42 * t2 * x34) - (x24 * t3 * x42 + x34 * t4 * x22 + x44 * t2 * x32) ) ) +
                 ( (-1 * x12) * ( (x21 * t3 * x44 + x31 * t4 * x24 + x41 * t2 * x34) - (x24 * t3 * x41 + x34 * t4 * x21 + x44 * t2 * x31) ) ) +
                 ( (1 * t1)   * ( (x21 * x32 * x44 + x31 * x42 * x24 + x41 * x22 * x34) - (x24 * x32 * x41 + x34 * x42 * x21 + x44 * x22 * x31) ) ) +
                 ( (-1 * x14) * ( (x21 * x32 * t4 + x31 * x42 * t2 + x41 * x22 * t3) - (t2 * x32 * x41 + t3 * x42 * x21 + t4 * x22 * x31) ) ) ;
           Dz = Dz / Ds;
             
           Dt =  ( (1 * x11)  * ( (x22 * x33 * t4 + x32 * x43 * t2  + x42 * x23 * t3) - (t2 * x33 * x42 + t3 * x43 * x22 + t4 * x23 * x32) ) ) +
                 ( (-1 * x12) * ( (x21 * x33 * t4 + x31 * x43 * t2 + x41 * x23 * t3) - (t2 * x33 * x41 + t3 * x43 * x21 + t4 * x23 * x31) ) ) +
                 ( (1 * x13)  * ( (x21 * x32 * t4 + x31 * x42 * t2 + x41 * x22 * t3) - (t2 * x32 * x41 + t3 * x42 * x21 + t4 * x22 * x31) ) ) +
                 ( (-1 * t1)  * ( (x21 * x32 * x43 + x31 * x42 * x23 + x41 * x22 * x33) - (x23 * x32 * x41 + x33 * x42 * x21 + x43 * x22 * x31) ) ) ;
            Dt = Dt / Ds;

            Console.WriteLine("Ds = " + Ds);
            Console.WriteLine("x = " + Dx);
            Console.WriteLine("y = " + Dy);
            Console.WriteLine("z = " + Dz);
            Console.WriteLine("t = " + Dt);
        }
    }


}

